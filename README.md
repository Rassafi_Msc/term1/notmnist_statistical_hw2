## How to use?

if you have not install make, virtualenv and python3.6 you need to :
```bash
sudo apt install make
sudo apt install virtualenv
sudo apt install python3.6
```
and then :
```bash
git clone https://gitlab.com/Rassafi_Msc/term1/notmnist_statistical_hw2.git
cd notmnist_statistical_hw2
make
source venv/bin/activate
python notmnist.py
```